use std::error::Error;
use std::fs;

pub mod cli;

pub fn run(config: &cli::Options) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename.as_ref().unwrap());

    let results = if config.case_sensitive {
        search(config.query.as_ref().unwrap(), contents.as_ref().unwrap())
    } else {
        search_case_insensitive(config.query.as_ref().unwrap(), contents.as_ref().unwrap())
    };

    for linematch in results {
        println!("{}", linematch);
    }

    Ok(())
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }

    results
}

fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }

    results
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "usty";
        let contents = "\
My terrible Code:
Long, Thick, Rusty.
Pick all three.";

        assert_eq!(vec!["Long, Thick, Rusty."], search(query, contents));
    }

    #[test]
    fn case_insensitive() {
        let query = "cOdE";
        let contents = "\
My terrible Code:
Long, Thick, Rusty.
Pick all three.
Wow this is bad code.";

        assert_eq!(
            vec!["My terrible Code:", "Wow this is bad code."],
            search_case_insensitive(query, contents)
        );
    }
}
