extern crate clap;

use clap::{App, Arg, crate_name, crate_version, crate_authors, crate_description};

pub struct Options {
    pub query: Option<String>,
    pub filename: Option<String>,
    pub case_sensitive: bool,
}

impl Default for Options {
    fn default() -> Options {
        Options {
            query: None,
            filename: None,
            case_sensitive: true,
        }
    }
}

impl Options {
    pub fn load() -> Options {
        let mut options = Options::default();

        let matches = App::new(crate_name!())
                        .version(crate_version!())
                        .author(crate_authors!())
                        .about(crate_description!())
                        .arg(Arg::with_name("ignorecase")
                            .short("i")
                            .long("ignore-case")
                            .takes_value(false)
                            .help("Ignore case distinction (case insensitive)"))
                        .arg(Arg::with_name("query")
                            .help("pattern to match")
                            .required(true)
                            .index(1))
                        .arg(Arg::with_name("INPUT")
                            .help("File to search")
                            .required(true)
                            .index(2))
                        .arg(Arg::with_name("verbose")
                            .short("v")
                            .multiple(true)
                            .help("Verbosity level"))
                        .get_matches();

        if matches.is_present("ignorecase") {
            options.case_sensitive = false;
        }

        // FIXME: Super gross generalized clone() but i don't know any better.
        options.filename = matches.value_of("INPUT").map(|i| i.to_owned());
        options.query = matches.value_of("query").map(|q| q.to_owned());

        // Just dump out settings when verbose is specified
        match matches.occurrences_of("v") {
            0 => {},
            1 | _ => {
                println!("VERBOSE: Input File: {:?}", options.filename);
                println!("VERBOSE: Query: {:?}", options.query);
            }
        }

        // Return options struct
        options
    }
}