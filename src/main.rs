use std::process;

use minigrep;
use minigrep::cli;

fn main() {

    let options = cli::Options::load();

    // Instead of unwrapping we just use "if let", since we don't
    // actively care about the return value, we just want to see if it failed.
    if let Err(e) = minigrep::run(&options) {
        eprintln!("Runtime Error: {}", e);
        process::exit(1);
    }
}
